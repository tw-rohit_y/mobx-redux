import React from 'react';

const Todo = ({
                  todos,
                  fetchTodos,
              }) => {
    return [
        <ul
            key={0}
        >
            <li>{todos[0]}</li>
        </ul>,
        <button
            key={1}
            onClick={fetchTodos}
        >
            Fetch todos
        </button>
    ];
};

export default Todo;
