import todoActionTypes from './todoActionTypes';

const initialState = {
    todos: ['learn redux'],
};

const todoReducer = (state = initialState, action) => {
    switch (action.type) {
        case todoActionTypes.FETCH_TODOS:
            return Object.assign({}, state, {
                    todos: ['learn reselect'],
                }
            );

        default:
            return state;
    }
};

export default todoReducer;
