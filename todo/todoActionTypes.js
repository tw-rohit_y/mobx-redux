const todoActionTypes = {
  FETCH_TODOS: 'FETCH_TODOS',
};

export default todoActionTypes;
