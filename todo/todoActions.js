import todoActionTypes from './todoActionTypes';

const fetchTodos = () => ({
    type: todoActionTypes.FETCH_TODOS
});

export {
    fetchTodos,
};
