import { connect } from 'react-redux';
import Todo from './Todo';
import {fetchTodos} from "./todoActions";

const mapStateToProps = (state) => ({
    todos: state.todos,
});

const mapDispatchToProps = (dispatch) => ({
    fetchTodos: () => {
        dispatch(fetchTodos());
    },
});

const TodoContainer = connect(mapStateToProps, mapDispatchToProps)(Todo);

export default TodoContainer;
