import React from "react";
import ReactDOM from "react-dom";

import {Provider} from 'react-redux';
import {createStore} from 'redux'

import TodoContainer from './todo/TodoContainer'
import todoReducer from './todo/todoReducer'

const app = document.getElementById('app');

const store = createStore(
    todoReducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

const App = () => (
    <Provider store={store}>
        <TodoContainer />
    </Provider>
);

ReactDOM.render(<App/>, app);
